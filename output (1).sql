 /* CREATE TABLE */
CREATE TABLE IF NOT EXISTS TABLE_NAME(
event_name VARCHAR( 100 ),
club_name VARCHAR( 100 ),
event_date VARCHAR( 100 )
);

/* INSERT QUERY */
INSERT INTO TABLE_NAME( event_name,club_name,event_date )
VALUES
(
    'icebreaking','iste','12-03-2024'
);

/* INSERT QUERY */
INSERT INTO TABLE_NAME( event_name,club_name,event_date )
VALUES
(
    'hAPPY EVENT','HAPPY CLUB','12-03-2024'
);

/* INSERT QUERY */
INSERT INTO TABLE_NAME( event_name,club_name,event_date )
VALUES
(
    'tekno','all clubs','11-03-2024'
);

/* INSERT QUERY */
INSERT INTO TABLE_NAME( event_name,club_name,event_date )
VALUES
(
    'technova','technke','12-04-2024'
);

/* INSERT QUERY */
INSERT INTO TABLE_NAME( event_name,club_name,event_date )
VALUES
(
    'Dancing Gala','Rythamic Thunders','12-03-2024'
);
