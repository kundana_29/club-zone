
\documentclass{beamer}
\usepackage{graphicx}
\usetheme{CambridgeUS}
\title{CLUBZONE}
\date{}
\institute{Team 10}
\begin{document}
\begin{frame}
   \texttt{\LARGE SHRI VISHNU ENGINEERING COLLEGE FOR WOMEN}
    \vspace{0.3cm}
    \centering
    \\
   \textbf{\Large CLUBZONE} 
    \vspace{0.2cm}
    \centering
    \\
    \texttt{\large TEAM 10}
     \vspace{0.2cm}
     \centering 
     \begin{table}
        \begin{center}
           \begin{tabular}{lll}
            {\small \textbf{Regd.No.}} & {\textbf{\small\centering Name}} & {\textbf{\small Work}}
            \\ \hline
 \small 22B01A42A0 &  \small SK.Mohisina &  \small Developer \\ 
  \small 22B01A4299 &  \small SK.Karishma &  \small Developer \\ 
  \small 22B01A4298 &  \small SK.Isha Firdous &  \small Database \\
  \small 22B01A42B5 &  \small V.Kundana &  \small GIT\\
  \small 22B01A42C3 &  \small Y.Harshitha &  \small Latex \\
  \hline
\end{tabular}  
        \end{center}         
\end{table}
\end{frame}
\begin{frame}{Introduction}
    \begin{itemize}
        \item \Large Its a project about various clubs present in shri vishnu engineering college for women.
        \item \Large All the clubs are gathered at one place with every piece of information about them.
        \item \Large This gives every detail about a pirtiucular club choosen by a student.
       
    \end{itemize}
\end{frame}
\begin{frame}[t]{Data flow Diagram}
    \begin{center} \includegraphics[width=0.8\textwidth,height=0.8\textheight,keepaspectratio]{student (1).png}  
    \end{center}
\end{frame}
\begin{frame}{Output for student login}
     \begin{center} \includegraphics[width=1.0\textwidth,height=1.0\textheight,keepaspectratio]{student login.png}  
    \end{center}
\end{frame}
\begin{frame}{Output for coordinator login}
     \begin{center} \includegraphics[width=1.0\textwidth,height=1.0\textheight,keepaspectratio]{student.png}  
    \end{center}  
\end{frame}
\begin{frame}{Conclusion}
    \begin{itemize}
        \item \Large Since all the clubs are gathered at one place its easy to access the information.
    \item \Large We will be updated about every event happeing in the college based on our interests.
    \item \Large We can manage our things in a better way.
    \end{itemize}
  \end{frame}
  \begin{frame}{Bibilography}
  \begin{itemize}
      \item \Large We took references from\\ "Python Crash Course" by Eric Matthes.\\
"Automate the Boring Stuff with Python" by Al Sweigart.\\
"Fluent Python" by Luciano Ramalho.
  \end{itemize}
      
  \end{frame}
\end{document}
