import tkinter as tk
from tkinter import ttk, messagebox
login_window = None
club_descriptions = {
    "Rythmic thunders": """RYTHIMIC THUNDERS
   It Is a Dancing Club of Svecw,We Do Dancing events and flash mobs,It Is a place where you can learn dance and enjoy a lot and we can create a beautiful memories here.
If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "style & slay": """STYLE AND SLAY 
   It Is a fashion Club of Svecw,We Do Modeling and design some trendy fashions and we conduct ramp walks basing on different themes.you can develop your fashion sense by joining here 
If you are intrested you can join us ,the contact details of coordinator are given below""",
    "Toast master": """TOAST MASTERS
 It is International club of svecw,where we you can learn communication skills and you can reduce your stage fear here by speaking and learn a lot of speaking skills
If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "idea club": """IDEA CLUB
 It is a idea generating club of svecw,where you can can use your innovative thinking to generate ideas,and you can implement your ideas in real time
If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "techpost": """TECH POST
 It is a technical club of svecw,where you can gain knowldge about new emerging technologies and you  writings will be published in magazine and link with some famous IIT and NIT
  If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "mathlets": """MATHELETS
    It is a maths club of svecw,it is under the maths department,we conduct some events releated to maths,you can learn a lot from this club
If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "rock me fab": """ROCK ME FAB
It is a organizing club of svecw,we can organize all events held in the svecw,by joining in this club you can develope your leader ship qualities and commanding skills
If you are intrested,you can join us ,the contact details of coordinator are given below.""",
    "Hobby horses": """HOBBY HORSES
It is a club of svecw,where you can make use of your hobbies,we conduct many intresting events,it contains of different hobbies such as drawing,mehandi,singing etc.
If you are intrested you can join us ,the contact details of coordinator are given below.""",
    "Music club": """MUSIC CLUB
It is a music club of svecw,it contains so many courses realted to music such as guitar,dmp,piano,violen,you will enjoy a lot
If you are intrested you can join us ,the contact details of coordinator are given below""",
    "organic forming": """ORGANIC FARMING
It is a greenary club of svecw where you can enjoy farming and it is totally organic,it is under the chemistry department.
here you can learn about different plants and their uses""",
}

club_details = {
    "Rythmic thunders": [
        ("ANUHYA", "9963166167", "AIML", "II"),
        ("SANIYA", "9849763422", "AIML", "II"),
        ("ISHA", "9482345678", "AIML", "II")
    ],
    "style & slay": [
        ("HARSHITHA", "9876543210", "AIDS", "II"),
        ("PRVALLIKA", "6543219870", "ECE", "II"),
        ("BANU", "9963166167", "CSE", "II")
    ],
    "Toast master": [
        ("ANJALI", "7689543210", "EEE", "III"),
        ("SUCHITHRA", "9867543211", "CSE", "II"),
        ("TEJASRI", "7834902318", "MECH", "II"),
    ],
    "idea club": [
        ("SAISREE", "9848505135", "ECE", "III"),
        ("JANAKI", "9345678923", "CSC", "II"),
        ("NEHA", "6754092312", "AIML", "II"),
    ],
    "techpost": [
        ("MEENAKSHI", "9824351756", "CSE", "III"),
        ("JANU", "7865943202", "CIVIL", "II"),
        ("YAKSHITHA", "9842431333", "AIDS", "II"),
    ],
    "mathlets": [
        ("SASI", "8769342190", "AIML", "III"),
        ("ANUSHA", "9846372310", "AIDS", "II"),
        ("SUPRIYA", "8341817677", "CSC", "II"),
    ],
    "rock me fab": [
        ("DURGA", "9290800878", "CIVIL", "II"),
        ("BHUMIKA", "939270766", "MECH", "III"),
        ("NAVYA", "9345762900", "AIDS", "II"),
    ],
    "Hobby horses": [
        ("ARUNA", "9456382910", "AIML", "II"),
        ("RUPA", "7684532972", "CSE", "II"),
        ("HARSHINI", "7968542310", "CSE", "III"),
    ],
    "Music club": [
        ("ALFIYA", "9848367851", "EEE", "II"),
        ("GAYATHRI", "9341786442", "ECE", "II"),
        ("RITHU", "9848505136", "IT", "III"),
    ],
    "organic forming": [
        ("KAVYA", "9673920152", "CSE", "II"),
        ("SIRI", "9076854309", "IT", "III"),
        ("NIHARIKA", "9043562781", "EEE", "II"),
    ],
}

def left_login():
    username = left_username_entry.get()
    password = left_password_entry.get()

    if username == "karishma" and password == "1234":
        messagebox.showinfo("Student Login Successful", "Welcome, " + username + "!")
        show_initial_page("STUDENT")

    else:
        messagebox.showerror("student Login Failed", "Invalid username or password")


def right_login():
    username = right_username_entry.get()
    password = right_password_entry.get()

    if username == "coordinator" and password == "pass123":
        messagebox.showinfo("Coordinator Login Successful", "Welcome, " + username + "!")
        coordinator_page()
    else:
        messagebox.showerror("Coordinator Login Failed", "Invalid username or password")


def show_initial_page(user_name):
    global login_window
    # Hide login widgets
    left_frame.pack_forget()
    right_frame.pack_forget()

    initial_page = tk.Frame(login_window, bg="grey")
    initial_page.pack(fill=tk.BOTH, expand=True)

    initial_label = tk.Label(initial_page, text=f"Welcome, {user_name}!", font=("Helvetica", 20))
    initial_label.pack(pady=20)

    club_options = ["Rythmic thunders", "style & slay", "Toast master", "idea club", "techpost", "mathlets",
                    "rock me fab", "Hobby horses", "Music club", "organic forming"]

    club_label = tk.Label(initial_page, text="Select a Club:")
    club_label.pack()

    club_combo = ttk.Combobox(initial_page, values=club_options)
    club_combo.pack()

    club_button = tk.Button(initial_page, text="Select Club",
                            command=lambda: select_club(user_name, club_combo.get(), initial_page))
    club_button.pack()

    back_button = tk.Button(initial_page, text="Back", command=lambda: back_to_login(initial_page))
    back_button.pack()


def select_club(user_name, club_name, previous_page):
    previous_page.pack_forget()

    select_club_page = tk.Frame(login_window, bg="grey")
    select_club_page.pack(fill=tk.BOTH, expand=True)

    club_description = club_descriptions.get(club_name, "Description not available")

    description_label = tk.Label(select_club_page, text=f"Club: {club_name}\nDescription: {club_description}",
                                 font=("Helvetica", 12))
    description_label.pack(pady=20)

    tree = ttk.Treeview(select_club_page, columns=("Name", "Phone", "Branch", "Year"), show="headings")
    tree.heading("Name", text="Coordinator Name")
    tree.heading("Phone", text="Phone Number")
    tree.heading("Branch", text="Branch")
    tree.heading("Year", text="Year")

    details = club_details.get(club_name, [])
    for detail in details:
        tree.insert("", "end", values=detail)

    tree.pack(padx=20, pady=20)

    back_button = tk.Button(select_club_page, text="Back", command=lambda: back_to_initial(user_name, select_club_page))
    back_button.pack()


def back_to_login(previous_page):
    previous_page.pack_forget()
    left_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
    right_frame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)


def back_to_initial(user_name, previous_page):
    previous_page.pack_forget()
    show_initial_page(user_name)


def coordinator_page():
    global login_window
    # Hide login widgets
    left_frame.pack_forget()
    right_frame.pack_forget()

    coordinator_page = tk.Frame(login_window, bg="grey")
    coordinator_page.pack(fill=tk.BOTH, expand=True)

    coordinator_label = tk.Label(coordinator_page, text="Coordinator Page", font=("Helvetica", 20))
    coordinator_label.pack(pady=20)

    # Add widgets for event name, event date, and eligibility criteria
    event_name_label = tk.Label(coordinator_page, text="Event Name:")
    event_name_label.pack()
    event_name_entry = tk.Entry(coordinator_page)
    event_name_entry.pack()

    event_date_label = tk.Label(coordinator_page, text="Event Date:")
    event_date_label.pack()
    event_date_entry = tk.Entry(coordinator_page)
    event_date_entry.pack()

    eligibility_label = tk.Label(coordinator_page, text="Eligibility:")
    eligibility_label.pack()
    eligibility_entry = tk.Entry(coordinator_page)
    eligibility_entry.pack()

    # Add a submit button
    submit_button = tk.Button(coordinator_page, text="Submit", command=lambda: submit_event_details(
        event_name_entry.get(), event_date_entry.get(), eligibility_entry.get()))
    submit_button.pack()

    back_button = tk.Button(coordinator_page, text="Back", command=lambda: back_to_login(coordinator_page))
    back_button.pack()


def submit_event_details(event_name, event_date, eligibility):
    # This function will handle the submission of event details
    messagebox.showinfo("Event Details", f"Event Name: {event_name}\nEvent Date: {event_date}\nEligibility: {eligibility}")


root = tk.Tk()
root.title("CLUBZONE")
root.iconbitmap(default="C:/Users/HP/Downloads/wiselogo (1).ico")

left_frame = tk.Frame(root, bg="skyblue", pady=250)
left_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

right_frame = tk.Frame(root, bg="lightpink", pady=250)
right_frame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

left_username_label = tk.Label(left_frame, text="Student name:")
left_username_entry = tk.Entry(left_frame)
left_password_label = tk.Label(left_frame, text="Student password:")
left_password_entry = tk.Entry(left_frame, show="*")

left_login_button = tk.Button(left_frame, text="Student login", command=left_login)

left_username_label.place(x=20, y=20)
left_username_entry.place(x=150, y=20)
left_password_label.place(x=20, y=60)
left_password_entry.place(x=150, y=60)
left_login_button.place(x=100, y=100)


right_username_label = tk.Label(right_frame, text="Coordinator name:")
right_username_entry = tk.Entry(right_frame)
right_password_label = tk.Label(right_frame, text="Coordinator name:")
right_password_entry = tk.Entry(right_frame, show="*")

right_login_button = tk.Button(right_frame, text="Coordinator login", command=right_login)

right_username_label.place(x=20, y=20)
right_username_entry.place(x=180, y=20)
right_password_label.place(x=20, y=60)
right_password_entry.place(x=180, y=60)
right_login_button.place(x=100, y=100)


root.geometry("800x600")

# Store the reference to the login window
login_window = root

root.mainloop()